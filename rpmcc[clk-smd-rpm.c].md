**#RPMCC driver** [clk-smd-rpm.c]

Open GCC driver for your downstream kernel  by default it should be located under following directory:  
**drivers/clk/qcom/clock-gcc-_SOC-NAME_.c**  
For 3.18/4.9 kernels:  
**drivers/clk/msm/clock-gcc-_SOC-NAME_.c**

Copy all defines with **RPM_SMD** in name.  

Example based on MSM8953 3.18 source:  
https://github.com/MiCode/Xiaomi_Kernel_OpenSource/blob/mido-n-oss/drivers/clk/msm/clock-gcc-8953.c  

Downstream code:  
```
DEFINE_CLK_RPM_SMD_BRANCH(xo_clk_src, xo_a_clk_src, RPM_MISC_CLK_TYPE, XO_ID, 19200000);
DEFINE_CLK_RPM_SMD(bimc_clk, bimc_a_clk, RPM_MEM_CLK_TYPE, BIMC_ID, NULL);
DEFINE_CLK_RPM_SMD(pcnoc_clk, pcnoc_a_clk, RPM_BUS_CLK_TYPE, PCNOC_ID, NULL);
DEFINE_CLK_RPM_SMD(snoc_clk, snoc_a_clk, RPM_BUS_CLK_TYPE, SNOC_ID, NULL);
DEFINE_CLK_RPM_SMD(sysmmnoc_clk, sysmmnoc_a_clk, RPM_BUS_CLK_TYPE, SYSMMNOC_ID, NULL);
DEFINE_CLK_RPM_SMD(ipa_clk, ipa_a_clk, RPM_IPA_CLK_TYPE, IPA_ID, NULL);
DEFINE_CLK_RPM_SMD_QDSS(qdss_clk, qdss_a_clk, RPM_MISC_CLK_TYPE, QDSS_ID);

/* SMD_XO_BUFFER */
DEFINE_CLK_RPM_SMD_XO_BUFFER(rf_clk2, rf_clk2_a, RF_CLK2_ID);
DEFINE_CLK_RPM_SMD_XO_BUFFER(rf_clk3, rf_clk3_a, RF_CLK3_ID);
DEFINE_CLK_RPM_SMD_XO_BUFFER(bb_clk1, bb_clk1_a, BB_CLK1_ID);
DEFINE_CLK_RPM_SMD_XO_BUFFER(bb_clk2, bb_clk2_a, BB_CLK2_ID);
DEFINE_CLK_RPM_SMD_XO_BUFFER(div_clk2, div_clk2_a, DIV_CLK2_ID);

DEFINE_CLK_RPM_SMD_XO_BUFFER_PINCTRL(bb_clk1_pin, bb_clk1_a_pin, BB_CLK1_ID);
DEFINE_CLK_RPM_SMD_XO_BUFFER_PINCTRL(bb_clk2_pin, bb_clk2_a_pin, BB_CLK2_ID);
```

To get it to mainline we need to change our define names.  

Here is translation table for defines: 


| Downstream  | Mainline |
| ------ | ------ |                
|DEFINE_CLK_RPM_SMD_BRANCH    |DEFINE_CLK_SMD_RPM_BRANCH|  
|DEFINE_CLK_RPM_SMD           |DEFINE_CLK_SMD_RPM|  
|DEFINE_CLK_RPM_SMD_QDSS      |DEFINE_CLK_SMD_RPM_QDSS|  
|DEFINE_CLK_RPM_SMD_XO_BUFFER |DEFINE_CLK_SMD_RPM_XO_BUFFER|  

After changing defines its time to replace third argument:  
| Downstream  | Mainline |
| ------ | ------ |   
|RPM_MISC_CLK_TYPE|  QCOM_SMD_RPM_MISC_CLK |  
|RPM_MEM_CLK_TYPE|     QCOM_SMD_RPM_MEM_CLK |  
|RPM_BUS_CLK_TYPE|     QCOM_SMD_RPM_BUS_CLK |  
|RPM_IPA_CLK_TYPE|     QCOM_SMD_RPM_IPA_CLK |  

Forth parmater which on downstream have ID in name is defined in different file:  
**include/dt-bindings/clock/msm-clocks-hwio-_SOC-NAME_.h**   
Example:  
https://github.com/MiCode/Xiaomi_Kernel_OpenSource/blob/mido-n-oss/include/dt-bindings/clock/msm-clocks-hwio-8953.h  
  
Just replace it with value in that, take a note values in file are written in 0x format which means Hexadecimal system so remember to change them to decimal before :)  
First argument of our new define must contain our soc codename.  

Now we can translate following downstream line by knowing QDSS_ID is equal to 0x1=1 :  
  
**DEFINE_CLK_RPM_SMD_QDSS(qdss_clk, qdss_a_clk, RPM_MISC_CLK_TYPE, QDSS_ID);**
->
**DEFINE_CLK_SMD_RPM_QDSS(msm8953, qdss_clk, qdss_a_clk, QCOM_SMD_RPM_MISC_CLK, 1);**

